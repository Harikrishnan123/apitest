Feature: Compare Two file api
  Background:

  Scenario: Api 1
    * url 'https://reqres.in/api/users/3'
    When method get
    Then status 200
    Then print response
    * def response1 = response
    * string json = response

  Scenario: Api 2
    * url 'https://reqres.in/api/users/2'
    When method get
    Then status 200
    * match response == response1

